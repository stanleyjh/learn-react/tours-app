# [Tours](https://stanleyjh.gitlab.io/learn-react/tours-app/)

This application shows a list of tours you can take. You can hide tours you are not interested in or read more about the tour. If you are not interested in any tours, there will be a button prompting you to refresh the page.

The data is referenced from an [external source](https://course-api.com/react-tours-project). Before the data loads, there is a loading screen that displays while the data is being fetched. Once the data is fetched, the loading screen will disappear. The data is pulled using an Asynchronous function and JavaScript's Fetch API. The API is also encapsulated in a try...catch block to handle exceptions.

The data is passed to each child component called Tour using prop drilling. The data is then accessed using destructuring and is displayed on the page using JSX.

## References

[John Smilga's Udemy React Course](https://www.udemy.com/course/react-tutorial-and-projects-course/?referralCode=FEE6A921AF07E2563CEF)
