import React, { useState, useEffect } from "react";
import Loading from "./Loading";
import Tours from "./Tours";

const url = "https://course-api.com/react-tours-project";
function App() {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  // removeTour function is here because the data is here.
  const removeTour = (id) => {
    const newTours = data.filter((data) => data.id !== id);
    setData(newTours);
  };

  const fetchTours = async () => {
    setLoading(true);

    try {
      const response = await fetch(url);
      const data = await response.json();
      setLoading(false);
      setData(data);
    } catch (error) {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchTours();
  }, []);

  // Shows before the data is fetched.
  if (loading) {
    return (
      <main>
        <Loading />
      </main>
    );
  }

  // Shows if all tours are removed.
  if (data.length === 0) {
    return (
      <main>
        <div className="title">
          <h2>no tours left</h2>
          <button className="btn" onClick={fetchTours}>
            refresh
          </button>
        </div>
      </main>
    );
  }

  // Shows after the data is fetched.
  return (
    <main>
      <Tours data={data} removeTour={removeTour} />
    </main>
  );
}

export default App;
